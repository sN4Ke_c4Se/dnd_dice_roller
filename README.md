# sN4Ke_c4Se DnD Dice Roller 🐉

```
                                      ___
                                   ➤-⎛- o⎞
  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \  ⎠
   sN4Ke_c4Se DnD Dice Roller         ⎠ /
  ⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎛ ⎛
  

```

## About

This script rolls virtual Dungeons & Dragons dice in your terminal! 

Rolls use standard D&D die types (4, 6, 8, 10, 12, 20, 100) but custom die types can be used with a script option. Multiple rolls can be added together. Modifiers are added to the total roll by default but can be added to each die rolled using a script option. The Advantage and Disadvantage options allow to roll a roll twice and return only the highest or the lowest result. Results are printed in the terminal but can also be saved to a text file using a script option.

Each roll must be passed in argument using the following pattern: 
`[dice quantity]` + `d` + `[die type]` + `[optional: modifier]` 

> For example: `1d20+2` or `3d4-1` or `2d6`

It is recommended to run the script with the option `--detail` the first time to see how rolls are being calculated.

## License 

You can use this script for noncommercial usage only, with attribution, following its [PolyForm Noncommercial License 1.0.0](https://polyformproject.org/licenses/noncommercial/1.0.0/)

## Requirements

This script does not use any third-party dependencies.

## Security Warning

The randomness used by this script is not suitable for passwords.

## Usage

1. This script must be run with at least one argument (roll). If no argument and no option is received, the script will display the usage message.

2. Multiple rolls can be added together using the option `PLUS` between each roll. 
> For example: `1d20+2 PLUS 2d6`
 
3. The option `--detail` can be use to see the result for each roll separately. It is recommended to use this the first time to get a better sense of how rolls are calculated.

4. Options can be mixed if compatible.

## Command-Line Usage & Options

##### In a terminal: 

`python3 [path_to sc_dice.py] [quantity]d[type][optional: +bonus/-malus] [optional: PLUS] [optional: -option(s)]`

##### Usage example: 

`python3 sc_dice.py 1d20+2 PLUS 2d6-2 --detail`

 >                1d20 -> Rolled   17
 >                 ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
 >                 Total:   17 +2 = 19
 >
 >                2d6 -> Rolled    3
 >                       Rolled    2
 >                       ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
 >                       Total:    5 -2 = 3
 >                ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯◉
 >                All Rolls Total:   22
                

##### Options available: 

> **NOTE1**:  If no argument is received, the script's usage is displayed.<br/>
> **NOTE2**:  The option `PLUS` can be added between each roll.<br/>
> **NOTE3**:  Multiple options can be used at once (if compatible).

`PLUS`             for Plus: Additions multiple types of roll together 
> For example: `2d6 PLUS 1d4`

`-AD` or `--advantage`    for Advantage: Rolls a roll twice and returns only the highest roll

`-DI` or `--disadvantage` for Disadvantage: Rolls a roll twice and returns only the lowest roll

`-c` or `--custom` for Custom: Allows custom types of die that aren't 4, 6, 8, 10, 12, 20, or 100

`-d` or `--detail` for Detail: Shows detailed calculation for each roll

`-e` or `--each`   for Each: Applies the bonus/malus to each rolled die instead of only to the total

`-h` or `--help`   for Help: Displays the script usage and exit

`-k` or `--keep`   for Keep: Keeps each result in a text file with timestamps

`-q` or `--quick`  for Quick Mode: Removes the rolling effect

`-s` or `--silent` for Silent Mode: Does not display any messages, only result


