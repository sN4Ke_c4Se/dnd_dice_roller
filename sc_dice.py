#!/usr/bin/env python3
################################################################################
##                                                                            ##
##  DND DICE ROLLER                                                           ##
##                                                                            ##
################################################################################

################################################################################
## LICENSE:                                                                   ##
################################################################################

"""
This software is licensed under the PolyForm Noncommercial License 1.0.0

In summary:
1. You may use, copy, share, or modify this code as long you respect its
noncommercial license.

2. You may use, copy, share, or modify this code as long you attribute it to its
original licensor by mentioning one of the 4-line Required Notices stated below.

3. You must indicate in comment if you have modified this code from its original
form by selecting the second modified Required Notice instead of the first one.

4. You can read the full license text here:
https://polyformproject.org/licenses/noncommercial/1.0.0

Include the following notice if you have not modified this code:

    Required Notice: Copyright @sN4Ke_c4Se (snake_case_software[at]pm.me | snakecase.software)
    Original source: https://gitlab.com/sN4Ke_c4Se/dnd_dice_roller
    Licensed under the PolyForm Noncommercial License 1.0.0
    (https://polyformproject.org/licenses/noncommercial/1.0.0)

If you have modified this code, include the following notice instead:

    Required Notice (modified): Copyright @sN4Ke_c4Se (snake_case_software[at]pm.me | snakecase.software)
    Modified from the original source: https://gitlab.com/sN4Ke_c4Se/dnd_dice_roller
    Licensed under the PolyForm Noncommercial License 1.0.0
    (https://polyformproject.org/licenses/noncommercial/1.0.0)

"""

################################################################################
## DESCRIPTION:                                                               ##
################################################################################

"""
Version: 1.1 (2023-10-02)

This program rolls virtual D&D dice in your terminal! 

At least one roll must be received in argument for the program to function but 
more can be added using a program option. 

Each roll must be formatted following the pattern: 
[dice quantity] + 'd' + [die type] + [modifier]
For example: 1d20+2 or 3d4-1

Roll(s) must use standard D&D die types (4, 6, 8, 10, 12, 20, 100) but custom 
die types can be used with a program option.

Each roll will be rolled randomly and modifier (bonus or malus) will be added if
applicable. If multiple rolls are added, a total will be printed. An option can
be added to add the modifier to each die rolled instead of the total rolled. If
the option Advantage or Disadvantage is selected, the roll will be rolled twice
and return either the highest or lowest result (this can be done on any die 
type). To understand better the options, it is recommended to first try the 
program with the options '--detail' added.

Results are printed in the terminal only unless the Keep option is selected 
which will additionally save the results to a text file created in the current
directory. Each time the program runs and the file is found, new results will be 
appended with a day and time header.

USAGE:

      python3 [path_to`sc_dice.py`] [quantity]['d'][type][optional: +bonus/-malus] [optional: PLUS] [optional: -option(s)]

  USAGE EXAMPLE: python3 sc_dice.py 1d20+2 PLUS 2d6-2 --detail

                 1d20 -> Rolled   17
                 ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
                 Total:   17 +2 = 19

                 2d6 -> Rolled    3
                        Rolled    2
                        ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
                        Total:    5 -2 = 3
                 ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯◉
                 All Rolls Total:   22

  NOTE1:  If no argument is received, the program's usage is displayed.
  NOTE2:  The option 'PLUS' can be added between each roll.
  NOTE3:  Multiple options can be used at once (if compatible).

  OPTIONS AVAILABLE:

  `PLUS`  for Plus: Additions multiple types of roll together (ex: 2d6 PLUS 1d4)

  `-AD` or `--advantage`    for Advantage: Rolls a roll twice and returns only the highest roll

  `-DI` or `--disadvantage` for Disadvantage: Rolls a roll twice and returns only the lowest roll

  `-c` or `--custom` for Custom: Allows custom types of die that aren't 4, 6, 8, 10, 12, 20, or 100

  `-d` or `--detail` for Detail: Shows detailed calculation for each roll

  `-e` or `--each`   for Each: Applies the bonus/malus to each rolled die instead of only to the total

  `-h` or `--help`   for Help: Displays the program usage and exit

  `-k` or `--keep`   for Keep: Keeps each result in a text file with timestamps

  `-q` or `--quick`  for Quick Mode: Removes the rolling effect

  `-s` or `--silent` for Silent Mode: Does not display any messages, only result

"""


################################################################################
### ALL IMPORTS                                                              ###
################################################################################

import re
import sys
import time
import random
from os.path import exists
from datetime import datetime 


################################################################################
### ALL CONSTANTS                                                            ###
################################################################################

FILE_PY_MAIN = "sc_dice.py"
FILE_TEXT_KEEP = "saved_dice_results.txt"

## VALID PROGRAM DICE TYPES: ###################################################
VALID_DICE = [4, 6, 8, 10, 12, 20, 100]

## PROGRAM OPTION VARIABLES: ###################################################
OP_PLUS = "Plus"
OP_ADVANTAGE = "Advantage"
OP_DISADVANTAGE = "Disadvantage"
OP_CUSTOM = "Custom"
OP_DETAIL = "Detail"
OP_EACH = "Each"
OP_HELP = "Help"
OP_KEEP = "Keep"
OP_QUICK = "Quick"
OP_SILENT = "Silent"
OP_PLUS_CLI = "PLUS"
OP_ADVANTAGE_CLI = "-AD"  ## <- Roll twice the roll and return the highest
OP_DISADVANTAGE_CLI = "-DI"  ## <- Roll twice the roll and return the lowest
OP_CUSTOM_CLI = "-c"  ## <- Custom type of die, if not (4, 6, 8, 10, 12, 20, 100)
OP_DETAIL_CLI = "-d"  ## <- Show all calculations
OP_EACH_CLI = "-e"  ## <- Add bonus/malus for each die rolled
OP_HELP_CLI = "-h"  ## <- Display usage
OP_KEEP_CLI = "-k"  ## <- Keep each result in a text file with timestamp
OP_QUICK_CLI = "-q"  ## <- Remove rolling effect
OP_SILENT_CLI = "-s"  ## <- Display no messages, only result
OP_ADVANTAGE_CLI_LONG = "--advantage"
OP_DISADVANTAGE_CLI_LONG = "--disadvantage"
OP_CUSTOM_CLI_LONG = "--custom"
OP_DETAIL_CLI_LONG = "--detail"
OP_EACH_CLI_LONG = "--each"
OP_HELP_CLI_LONG = "--help"
OP_KEEP_CLI_LONG = "--keep"
OP_QUICK_CLI_LONG = "--quick"
OP_SILENT_CLI_LONG = "--silent"
OP_VALID = [OP_PLUS_CLI, \
            OP_ADVANTAGE_CLI, \
            OP_DISADVANTAGE_CLI, \
            OP_CUSTOM_CLI, \
            OP_DETAIL_CLI, \
            OP_EACH_CLI, \
            OP_HELP_CLI, \
            OP_KEEP_CLI, \
            OP_QUICK_CLI, \
            OP_SILENT_CLI, \
            OP_ADVANTAGE_CLI_LONG, \
            OP_DISADVANTAGE_CLI_LONG, \
            OP_CUSTOM_CLI_LONG, \
            OP_DETAIL_CLI_LONG, \
            OP_EACH_CLI_LONG, \
            OP_HELP_CLI_LONG, \
            OP_KEEP_CLI_LONG, \
            OP_QUICK_CLI_LONG, \
            OP_SILENT_CLI_LONG]

## PROGRAM HEADER: #############################################################
MSG_HEADER = '''                                                  ___
                                               ➤-⎛- o⎞
    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾    \\  ⎠
    sN4Ke_c4Se DnD Dice Roller                    ⎠ /
    ⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎛ ⎛'''

## MESSAGE TYPES (with style): #################################################
STR_ERROR = "  \033[39;1;49mERROR!-> \033[0m" ## <- Bold
STR_NOTICE = "\n  \033[39;1;49mNOTICE-> \033[0m" ## <- Bold

## PROGRAM USAGE: ##############################################################
MSG_USAGE = f'''\
  \033[39;1;49mUSAGE: python3 [path_to`{FILE_PY_MAIN}`] [quantity]['d'][type][optional: +bonus/-malus] [optional: {OP_PLUS_CLI}] [optional: -option(s)]\033[0m

  \033[39;1;49mUSAGE EXAMPLE:\033[0m python3 {FILE_PY_MAIN} 1d20+2 {OP_PLUS_CLI} 2d6-2 {OP_DETAIL_CLI_LONG}
                  
                 1d20 -> Rolled   17
                         ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
                         Total:   17 +2 = 19

                 2d6 -> Rolled    3
                        Rolled    2
                        ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯
                        Total:    5 -2 = 3
                 ⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯◉
                 All Rolls Total:   22


  \033[39;1;49mNOTE1:\033[0m  If no argument is received, the program's usage is displayed.
  \033[39;1;49mNOTE2:\033[0m  The option '{OP_PLUS_CLI}' can be added between each roll.
  \033[39;1;49mNOTE3:\033[0m  Multiple options can be used at once (if compatible).

  \033[39;1;49mOPTIONS AVAILABLE:\033[0m

  `{OP_PLUS_CLI}`             for Plus: Additions multiple types of roll together (ex: 2d6 {OP_PLUS_CLI} 1d4)      

  `{OP_ADVANTAGE_CLI}` or `{OP_ADVANTAGE_CLI_LONG}` for Advantage: Rolls a roll twice and returns only the highest roll

  `{OP_DISADVANTAGE_CLI}` or `{OP_DISADVANTAGE_CLI_LONG}` for Disadvantage: Rolls a roll twice and returns only the lowest roll

  `{OP_CUSTOM_CLI}` or `{OP_CUSTOM_CLI_LONG}` for Custom: Allows custom types of die that aren't 4, 6, 8, 10, 12, 20, or 100 

  `{OP_DETAIL_CLI}` or `{OP_DETAIL_CLI_LONG}` for Detail: Shows detailed calculation for each roll

  `{OP_EACH_CLI}` or `{OP_EACH_CLI_LONG}`   for Each: Applies the bonus/malus to each rolled die instead of only to the total 

  `{OP_HELP_CLI}` or `{OP_HELP_CLI_LONG}`   for Help: Displays the program usage and exit

  `{OP_KEEP_CLI}` or `{OP_KEEP_CLI_LONG}`   for Keep: Keeps each result in a text file with timestamps

  `{OP_QUICK_CLI}` or `{OP_QUICK_CLI_LONG}`  for Quick Mode: Removes the rolling effect

  `{OP_SILENT_CLI}` or `{OP_SILENT_CLI_LONG}` for Silent Mode: Does not display any messages, only result
'''

## GENERAL MESSAGES: ###########################################################
MSG_EXIT = "  The program will now exit.\n"
MSG_FILE_CREATED_F = STR_NOTICE + \
"A new file was created with the saved results at: {}"
MSG_FILE_UPDATED_F = STR_NOTICE + \
"The file was updated with the saved results at: {}"
MSG_ADVANTAGE = "  Advantage! The highest roll is: \n"
MSG_DISADVANTAGE = "  Disadvantage... The lowest roll is: \n"

## ERROR MESSAGES: #############################################################
ERR_NO_VALID_ROLL = STR_ERROR + \
"No valid roll was received in arguments.\n" \
+ "  Restart the program with at least one valid dice roll (number+type EX: 1d20)."

ERR_UNUSUAL_DIE_TYPE_F = STR_ERROR + \
"The die type for the roll '{}' is not a usual D&D die type.\n" \
+ "  To use a custom D&D die types, add the option '{}' to the command."

ERR_MULTIPLE_ROLLS = STR_ERROR + \
f"No option '{OP_PLUS_CLI}' was used between rolls in arguments.\n" \
+ "  Only the first roll will be considered.\n"

ERR_WRITING_FILE_F = STR_ERROR + \
"An error occurred while creating or updating the file: {}" 

################################################################################
### CUSTOM EXCEPTIONS                                                        ###
################################################################################

class NoValidRoll(Exception):
    """Raised when no valid roll was received in arguments."""
    pass


################################################################################
### ALL FUNCTIONS                                                            ###
################################################################################

def format_total_of_rolls(total_rolls, options, results_string):
    """Return a string of the total of all the rolls if multiple rolls were 
    received with the Plus option."""

    if silent:
        return f"\nTotal: {total_rolls}"
    else:
        total_rolls_string = f"  All Rolls Total: \033[39;1;49m{total_rolls:>4}\033[0m"
        total_line = "  " + ("⎯" * (len(total_rolls_string) -2)) + "◉" + "\n"
        if OP_DETAIL_CLI in options:
            lines = [len(line) for line in results_string.split("\n")]
            max_lenght = max(lines)
            total_line = "\n  " + ("⎯" * (max_lenght -10)) + "◉" + "\n"
        return "\n" + total_line + total_rolls_string

def print_program_usage_and_exit(args, help):
    """Print the program usage and exit if the help option was selected or if no 
    option was received in arguments."""

    if not args or help:
        print()
        print(MSG_USAGE)
        sys.exit()

def print_rolling_effect():
    """Print a delayed rolling effect."""

    rolling = "... ◇ ◻︎◇△⎔▫︎△⎔◇❏ ↓"
    sys.stdout.write("  Rolling")
    sys.stdout.flush()
    time.sleep(0.2)
    sys.stdout.write(" dice!")
    sys.stdout.flush()
    time.sleep(0.15)
    for char in rolling:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.025)
    print("\n")

def print_verbose_messages(*messages):
    """Print all messages received if Silent Mode was not selected."""

    if silent == False:
        for message in messages:
            print(message)

def roll_twice_and_compare(roll):
    """Return either the highest roll or the lowest roll of two depending on the
    circumstance selected (Advantage or Disadvantage option). Print messages
    corresponding to the result. Print both compared rolls if the Detail option
    was selected."""

    first_roll = roll
    first_result = first_roll.roll_dice() + "\n"
    first_total = first_roll.result
    first = {'result': first_result, 'total': first_total}
    ## Copy the roll and roll again: 
    second_roll = roll 
    second_result = second_roll.roll_dice() + "\n"
    second_total = second_roll.result
    second = {'result':second_result, 'total':second_total}
    ## Compare two rolls for highest and lowest:
    highest_roll = None
    lowest_roll = None
    if first['total'] >= second['total']:
        highest_roll = first['result']
        lowest_roll = second['result']
    elif first['total'] < second['total']:
        highest_roll = second['result']
        lowest_roll = first['result']
    if roll.detail:
        print(first['result'])
        print_verbose_messages("  Compared with " + ("⎼" * 26) + "\n")
        print(second['result'])
        print_verbose_messages("  " + ("‾" * 40) + "\n")
    ## Return roll according to circumstances:
    if roll.circumstances == OP_ADVANTAGE:
        print_verbose_messages(MSG_ADVANTAGE)
        return highest_roll
    if roll.circumstances == OP_DISADVANTAGE:
        print_verbose_messages(MSG_DISADVANTAGE)
        return lowest_roll
        
def save_results_to_file(results_string):
    """Save the roll(s) result(s) to a text file with a timestamp header."""

    ## Format results string:
    results_string = results_string.replace("⎯", "")
    results_string = results_string.replace("◉", "")
    results_string = results_string.replace('\033[39;1;49m', '')
    results_string = results_string.replace('\033[0m', '')
    ## Format roll header:
    current_time = datetime.now()
    time_string = current_time.strftime(" Year %Y, on %B the %d, a %A at %H:%M:%S")
    top_line = "⎻" * 60 + "\n"
    bottom_line = "⎼" * 60 + " ⎔△◻︎◇" + "\n\n"
    ## Print to file:
    results_string = "\n" + top_line + time_string + "\n" + bottom_line + results_string
    write_to_file(FILE_TEXT_KEEP, results_string)

def validate_arguments(args):
    """Return a list of arguments and variables help, keep, quick, and 
    silent."""

    args = sys.argv[1:]
    help = False
    keep = False
    quick = False
    silent = False
    if OP_HELP_CLI in args or OP_HELP_CLI_LONG in args:
        help = True
    if OP_KEEP_CLI in args or OP_KEEP_CLI_LONG in args:
        keep = True   
    if OP_QUICK_CLI in args or OP_QUICK_CLI_LONG in args:
        quick = True
    if OP_SILENT_CLI in args or OP_SILENT_CLI_LONG in args:
        silent = True
    ## Remove silent and help flags from args:
    args = [arg for arg in args if arg != OP_SILENT_CLI \
                                    and arg != OP_SILENT_CLI_LONG \
                                    and arg != OP_HELP_CLI \
                                    and arg != OP_HELP_CLI_LONG]
    return args, help, keep, quick, silent

def validate_options(args):
    """Return a list of valid program options. Return an empty list if none."""

    options = []
    for arg in args:
        if arg in OP_VALID:
            options.append(arg)
    ## Remove Advantage and Disadvantage if both are in options:
    if (OP_ADVANTAGE_CLI in options or OP_ADVANTAGE_CLI_LONG in options) \
    and (OP_DISADVANTAGE_CLI in options or OP_DISADVANTAGE_CLI_LONG in options):
        options = [opt for opt in options if opt not in [OP_ADVANTAGE_CLI, \
                                                         OP_ADVANTAGE_CLI_LONG, \
                                                         OP_DISADVANTAGE_CLI, \
                                                         OP_DISADVANTAGE_CLI_LONG]]
    return options

def validate_rolls(args):
    """Return a list of valid rolls from arguments."""

    rolls = []
    try:
        ## Match roll pattern and is not zero die:                            
        valid_roll =  r'\b[0-9]+[d]{1}[0-9]+([+-]{1}[0-9]+)?\b'
        for arg in args:
            if (re.fullmatch(valid_roll, arg)):
                if arg[:1] != "0":
                    rolls.append(arg)
        if rolls: 
            return rolls
        raise NoValidRoll
    except NoValidRoll:
        print_verbose_messages(ERR_NO_VALID_ROLL, MSG_EXIT)
        sys.exit()

def write_to_file(file_name, text):
    """Create or update a file from the received file name and text. Print a 
    corresponding confirmation message."""

    try:
        if not exists(file_name):
            with open(file_name, "w") as file:
                text = text + "\n"
                file.write(text)
                print_verbose_messages(MSG_FILE_CREATED_F.format(file_name))
        else:
            with open(file_name, "a") as file:
                text = "\n" + text + "\n"
                file.write(text)
                print_verbose_messages(MSG_FILE_UPDATED_F.format(file_name))
    except IOError:
        print_verbose_messages("\n", ERR_WRITING_FILE_F.format(file_name))
        sys.exit()


################################################################################
### CLASSES AND METHODS                                                      ###
################################################################################

class Roll():

    def __init__(self, roll, options):
        """Create a Roll object and attribute all variables according to 
        argument(s) received and option(s) selected."""

        self.quantity = int(roll.split('d')[0]) ## <- int
        self.die_type = self.validate_die_type(roll, options) ## <- int
        self.modifier = None ## <- int
        self.each = False
        self.validate_modifier(roll, options)
        self.detail = False
        if OP_DETAIL_CLI in options or OP_DETAIL_CLI_LONG in options:
            self.detail = True 
        self.circumstances = None
        if OP_ADVANTAGE_CLI in options or OP_ADVANTAGE_CLI_LONG in options:
            self.circumstances = OP_ADVANTAGE
        if OP_DISADVANTAGE_CLI in options or OP_DISADVANTAGE_CLI_LONG in options:
            self.circumstances = OP_DISADVANTAGE
        self.roll = self.format_roll()
        self.result = None ## <- int


    def add_roll_effects(self, roll):
        """Return a string with some effect for a success or a failure."""

        result = f"Rolled {roll:>4}"
        if self.die_type == 20 and roll == 20:
            result = f"Rolled {roll:>4}! NATURAL 20!✨"
        elif self.die_type == 20 and roll == 1:
            result = f"Rolled {roll:>4} ...fumble ☹︎"
        elif self.die_type == roll:
            result = f"Rolled {roll:>4}  Maximum!☻"
        elif roll == 1:
            result = f"Rolled {roll:>4}  ☹︎"
        return result

    def add_sign_to_modifier_string(self):
        """Return a string with a modifier, add a plus sign if positif."""

        modifier = ""
        if self.modifier:
            modifier = str(self.modifier)
            if self.modifier > 0:
                modifier = "+" + modifier
        return modifier

    def format_roll(self):
        """Format a roll in a printable format."""
 
        roll = str(self.quantity) + 'd' + str(self.die_type)
        if self.modifier:
            if self.each:
                return roll + f" + ({self.modifier}x{str(self.quantity)})"
            modifier = str(self.modifier)
            if self.modifier > 0:
                modifier = "+" + modifier
            return roll + modifier
        return roll

    def format_roll_results(self, dice_rolls, total):
        """Return a formatted string of the roll results according to the 
        selected program options."""

        modifier = self.add_sign_to_modifier_string()
        results_list = [f"{self.add_roll_effects(roll)}\n" for roll in dice_rolls]
        result_string = "".join(results_list)
        ## If option Detail is selected:
        if self.detail:
            tab = " " * (len(str(self.quantity)) + len(str(self.die_type)) + 7)
            if self.each:
                results_list = [f"{tab}{self.add_roll_effects(roll)}\n"\
                                .replace(str(roll), f"{roll} {modifier}")\
                                    for roll in dice_rolls]
                if self.modifier:                 
                    total_string = f"{tab}Total: {sum(dice_rolls):>4} ({modifier}x{self.quantity}) = \033[39;1;49m{total}\033[0m"
                else:
                    total_string = f"{tab}Total: \033[39;1;49m{sum(dice_rolls):>4}\033[0m"
            else:
                results_list = [f"{tab}{result}" for result in results_list]
                if self.modifier:
                    total_string = f"{tab}Total: {sum(dice_rolls):>4} {modifier} = \033[39;1;49m{total}\033[0m"
                else:
                    total_string = f"{tab}Total: \033[39;1;49m{sum(dice_rolls):>4}\033[0m"
            ## Join results, add divider, add total line:
            result_string = "".join(results_list)
            total_line = tab + ("⎯" * (len(total_string) - len(tab) - 10)) + "\n"
            total_string = total_string.replace("(+", "+(")
            total_string = total_string.replace("(-", "-(")
            result_string += total_line + total_string
            result_string = f"  {self.quantity}d{self.die_type} -> {result_string.strip()}"
        ## If not silent but not detailed:
        else:
            result_string = f"  Result for {self.roll:>7} = \033[39;1;49m{str(total):>4}\033[0m"
            result_string = result_string.replace("(+", "+ (")
            result_string = result_string.replace("+ (-", "- (")
            if self.die_type == 20:
                naturals = [nat for nat in dice_rolls if nat == 20]
                fumbles = [fum for fum in dice_rolls if fum == 1]
                if naturals or fumbles:
                    line_including = "\n  Including"
                    line_naturals = f"  Number of natural 20s: {len(naturals)}\n"
                    if naturals:
                        line_naturals = line_naturals.replace("\n", " NATURAL 20!✨\n")
                    line_fumbles = f"  Number of fumbles (1): {len(fumbles)}"
                    if fumbles:
                        line_fumbles += "  ☹︎"
                    line = ("." * (len(line_naturals) - 12)) + "\n"
                    result_string += line_including + line + line_naturals + line_fumbles + "\n"
        return result_string

    def roll_dice(self):
        """Return a formatted printable result after rolling each die in the 
        roll. Add modifier following options, if applicable."""

        total = None 
        dice_rolls = []
        ## Roll die and append to list each roll result:
        for die in range(self.quantity):
            roll = random.randint(1, self.die_type)
            dice_rolls.append(roll)
        ## Calculate total (with modifier if applicable):
        if self.each and self.modifier:
            total = sum(dice_rolls) + (self.modifier * self.quantity)
        elif self.modifier:
            total = sum(dice_rolls) + self.modifier
        else:
            total = sum(dice_rolls)
        self.result = total
        ## If option Silent is selected:
        if silent:
            return str(total)
        else:
            return self.format_roll_results(dice_rolls, total)
                   
    def validate_die_type(self, roll, options):
        """Return a valid die type or a custom die type if the custom option was
        selected."""

        die_type = int(re.findall('d([0-9]+)', roll)[0])
        if die_type in VALID_DICE \
           or OP_CUSTOM_CLI in options \
           or OP_CUSTOM_CLI_LONG in options:
            return die_type
        print_verbose_messages(ERR_UNUSUAL_DIE_TYPE_F\
                                .format(roll, OP_CUSTOM_CLI_LONG), MSG_EXIT)
        sys.exit()
      
    def validate_modifier(self, roll, options):
        """Validate and attribute modifier following received option, if 
        applicable."""

        if OP_EACH_CLI in options or OP_EACH_CLI_LONG in options:
            self.each = True
        if '+' in roll or '-' in roll:
            self.modifier = int(re.findall(r'([+-][0-9]+)', roll)[0])

## PROGRAM MAIN: ###############################################################
def main():
    """
    1. Collect and validate the program's option(s) and argument(s). Validate 
    each roll received in argument and ignore incorrectly formatted rolls. 
    
    2. If multiple rolls were collected but the Plus option was not found, only
    consider the first roll collected.

    3. Print a delayed rolling dice effect to add suspense, if the Silent option
    or the Quick option was not selected.

    4. Create a Roll object for each valid roll received in arguments and 
    cumulate the results and total formatted following to the option(s) 
    received.

    5. Print the formatted results.

    6. If the Keep option was selected, save the results to a text file with a
    formatted timestamp header.

    """

    ## Collect option Silent Mode, option Help, and argument(s):
    global silent
    args, help, keep, quick, silent = validate_arguments(sys.argv)

    ## If no option or if the help option was selected:
    print_program_usage_and_exit(args, help)
    print_verbose_messages("")

    ## Validate roll(s) and option(s), if applicable:
    rolls = validate_rolls(args)
    options = validate_options(args) 

    ## Do not add rolls if the option Plus was not selected:
    if len(rolls) > 1 and not OP_PLUS_CLI in options:
        print_verbose_messages(ERR_MULTIPLE_ROLLS)
        rolls = rolls[:1]

    ## Delayed rolling effect, if not Quick mode or not Silent mode:
    if not quick and not silent:
        print_rolling_effect()

    ## Roll all rolls and print result(s):
    results_string = ""
    total_rolls = 0
    for roll in rolls:
        roll = Roll(roll, options)
        ## If option Advantage or Disadvantage is selected:
        if roll.circumstances:
            roll_result_compared = roll_twice_and_compare(roll)
            results_string += roll_result_compared
        else:
            roll_result = roll.roll_dice() + "\n"
            results_string += roll_result
        if roll.detail:
            results_string += "\n"
        total_rolls += roll.result
    results_string = results_string.rstrip()
    if OP_PLUS_CLI in options and len(rolls) > 1:
        results_string += format_total_of_rolls(total_rolls, options, results_string)
    print(results_string)

    ## Keep all results to file if Keep option selected:
    if keep:
        save_results_to_file(results_string)
    print_verbose_messages("")


################################################################################

if __name__ == "__main__":
    main()
